<?php
    include 'presentacion/encabezado.php';
?>

<div class="container">

    <div class="row mt-3">
        <!-- Imagen logo -->
        <div class="col-xs-12 col-lg-4 text-center"> 

        </div>
        
        <div class="col-xs-12 col-lg-8 text-center">     
            <form>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1">
                </div>
                <div class="mb-3 form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-xs-12 col-lg-2 text-center"> <a href="#">Ingresar</a> | <a href="#">Registro</a> </div>
    </div>

</div>
