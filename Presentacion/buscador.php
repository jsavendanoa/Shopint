<div class="container">

    <div class="row mt-3">
        <div class="col-3 text-center"> 
            <select class="form-select" aria-label="Default select example">
                <option selected>Origen</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
        </div>

        <div class="col-3 text-center"> 
            <select class="form-select" aria-label="Default select example">
                <option selected>Destino</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
        </div>

        <div class="col-2 text-center"> 
            <div class="input-group mb-3">
                <span class="input-group-text">Fecha ida</span>
                <input type="date" class="form-control" placeholder="Fecha">
            </div>
        </div>

        <div class="col-2 text-center"> 
            <div class="input-group mb-3">
                <span class="input-group-text">Fecha regreso</span>
                <input type="date" class="form-control" placeholder="Fecha">
            </div>
        </div>

        <div class="col-2 text-center"> 
            <button type="button" class="btn btn-primary">Buscar</button>
        </div>

</div>